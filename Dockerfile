# Use a lightweight base image
FROM nginx:alpine

# Copy the static HTML files to the appropriate location
COPY hello-world.html /usr/share/nginx/html/index.html

# Expose the default Nginx port
EXPOSE 80

# Start Nginx server when the container starts
CMD ["nginx", "-g", "daemon off;"]
